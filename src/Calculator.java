import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class Calculator extends JFrame implements ActionListener {

    private JButton b1, b2, b3, b4, b5, b6, b7, b8, b9, b0, bComma, bResult, bAdd, bSubtract, bDivine, bMultiple, bCleanAll, bEmpty1,bEmpty2;
    private TextField tInput;

    private String numberFromDigits = "";
    private String numberAsString ="0";
    private Double numberInMemoryAsDouble = 0.0;
    private Double result=0.0;
    private String typeOfActivity="";

    public Calculator() {
        setTitle("Calculator");
        setSize(216, 339);
        setResizable(false);

        b1 = new JButton("1");
        b1.setBounds(0, 100, 50, 50);
        b1.addActionListener(this);
        add(b1);

        b2 = new JButton("2");
        b2.setBounds(50, 100, 50, 50);
        b2.addActionListener(this);
        add(b2);

        b3 = new JButton("3");
        b3.setBounds(100, 100, 50, 50);
        b3.addActionListener(this);
        add(b3);

        b4 = new JButton("4");
        b4.setBounds(0, 150, 50, 50);
        b4.addActionListener(this);
        add(b4);

        b5 = new JButton("5");
        b5.setBounds(50, 150, 50, 50);
        b5.addActionListener(this);
        add(b5);

        b6 = new JButton("6");
        b6.setBounds(100, 150, 50, 50);
        b6.addActionListener(this);
        add(b6);

        b7 = new JButton("7");
        b7.setBounds(0, 200, 50, 50);
        b7.addActionListener(this);
        add(b7);

        b8 = new JButton("8");
        b8.setBounds(50, 200, 50, 50);
        b8.addActionListener(this);
        add(b8);

        b9 = new JButton("9");
        b9.setBounds(100, 200, 50, 50);
        b9.addActionListener(this);
        add(b9);

        b0 = new JButton("0");
        b0.setBounds(0, 250, 50, 50);
        b0.addActionListener(this);
        add(b0);

        bComma = new JButton(".");
        bComma.setBounds(100, 250, 50, 50);
        bComma.addActionListener(this);
        add(bComma);

        bResult = new JButton("=");
        bResult.setBounds(50, 250, 50, 50);
        bResult.addActionListener(this);
        add(bResult);


        bAdd = new JButton("+");
        bAdd.setBounds(150, 100, 50, 50);
        bAdd.addActionListener(this);
        add(bAdd);

        bSubtract = new JButton("-");
        bSubtract.setBounds(150, 150, 50, 50);
        bSubtract.addActionListener(this);
        add(bSubtract);

        bMultiple = new JButton("x");
        bMultiple.setBounds(150, 200, 50, 50);
        bMultiple.addActionListener(this);
        add(bMultiple);

        bDivine = new JButton("/");
        bDivine.setBounds(150, 250, 50, 50);
        bDivine.addActionListener(this);
        add(bDivine);

        bCleanAll = new JButton("CE");
        bCleanAll.setBounds(50, 50, 100, 50);
        bCleanAll.addActionListener(this);
        add(bCleanAll);

        bEmpty1 = new JButton("");
        bEmpty1.setBounds(0,50,50,50);
        add(bEmpty1);

        bEmpty2 = new JButton("");
        bEmpty2.setBounds(150,50,50,50);
        add(bEmpty2);

        tInput = new TextField();
        tInput.setBounds(0, 0, 200, 50);
        add(tInput);



    }

    /**
     * Main method for buttons.
     * @param e Object that created action.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == bAdd) {
            movingNumberToNumberInMemoryAsDouble();
            this.typeOfActivity = bAdd.getText();
        } else if (source == bSubtract) {
            movingNumberToNumberInMemoryAsDouble();
            this.typeOfActivity = bSubtract.getText();
        } else if (source == bDivine) {
            movingNumberToNumberInMemoryAsDouble();
            this.typeOfActivity = bDivine.getText();
        } else if (source == bMultiple) {
            movingNumberToNumberInMemoryAsDouble();
            this.typeOfActivity = bMultiple.getText();
        }  else if (source == bCleanAll) {
            this.numberInMemoryAsDouble = 0.0;
            this.numberFromDigits = "";
            tInput.setText(numberFromDigits);
        } else if (source == bResult) {
            setResult();
            tInput.setText(Double.toString(result));
            this.numberAsString =Double.toString(result);
        } else {
            makeNumberWithButtons(e);
        }
    }

    /**
     * Method for making numberFromDigits while using buttons.
     * @param button Object that created action.
     */
    private void makeNumberWithButtons(ActionEvent button) {
        Object source = button.getSource();
        if (source == b1) {
            makeNumberAndNumberAsString(b1);
        } else if (source == b2) {
            makeNumberAndNumberAsString(b2);
        } else if (source == b3) {
            makeNumberAndNumberAsString(b3);
        } else if (source == b4) {
            makeNumberAndNumberAsString(b4);
        } else if (source == b5) {
            makeNumberAndNumberAsString(b5);
        } else if (source == b6) {
            makeNumberAndNumberAsString(b6);
        } else if (source == b7) {
            makeNumberAndNumberAsString(b7);
        } else if (source == b8) {
            makeNumberAndNumberAsString(b8);
        } else if (source == b9) {
            makeNumberAndNumberAsString(b9);
        } else if (source == b0) {
            makeNumberAndNumberAsString(b0);
        } else if (source == bComma) {
            makeNumberAndNumberAsString(bComma);
        }


    }

    /**
     * Method for single button to make numberFromDigits and make numberAsString from numberFromDigits.
     * @param button  Button used to make numberFromDigits.
     */
    private void makeNumberAndNumberAsString(JButton button) {
        this.numberFromDigits = numberFromDigits + button.getText();
        tInput.setText(numberFromDigits);
        this.numberAsString ="0" + numberFromDigits;
    }

    /**
     * Moving numberFromDigits to numberInMemoryAsDouble to clear numberFromDigits.
     */
    private void movingNumberToNumberInMemoryAsDouble() {
        if(numberFromDigits.equals("")){
            numberFromDigits ="0";
            this.numberInMemoryAsDouble = Double.parseDouble(numberAsString);
            this.numberFromDigits = "";
        } else {
            this.numberInMemoryAsDouble = Double.parseDouble(numberAsString);
            this.numberFromDigits = "";
        }

    }

    /**
     * Method to set result from numberFromDigits and numberInMemoryAsDouble.
     * If numberFromDigits is "", it must be fill with "0" or parseDouble will not work.
     */
    private void setResult (){

           if (typeOfActivity.equals("+")){
               this.result = numberInMemoryAsDouble + (Double.parseDouble(numberAsString));
           } else if (typeOfActivity.equals("-")){
               this.result = numberInMemoryAsDouble - (Double.parseDouble(numberAsString));
           } else if (typeOfActivity.equals("x")){
               this.result = numberInMemoryAsDouble * (Double.parseDouble(numberAsString));
           } else if (typeOfActivity.equals("/")){
               this.result = numberInMemoryAsDouble / (Double.parseDouble(numberAsString));
           } else if (typeOfActivity.equals("")) {
               this.result=0.0;
           }
       }


    }






