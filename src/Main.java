import javax.swing.*;

public class Main {
    public static void main(String[] args) {

        Calculator calculator = new Calculator();
        calculator.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        calculator.setVisible(true);

    }
}
